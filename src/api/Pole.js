import request from "@/utils/request";

//一体杆列表
export function getPoleListApi(params) {
  return request({
    url: "/pole/info/list",
    params,
  });
}

//获取关联区域列表
export function getAreaDropListApi() {
  return request({
    url: "/parking/area/dropList",
  });
}

//添加一体杆
export function insertPoleApi(data) {
  return request({
    url: "/pole/info",
    method: "POST",
    data,
  });
}

//删除一体杆
export function deletePoleApi(ids) {
  return request({
    url: `/pole/info/${ids}`,
    method: "delete",
  });
}

//编辑一体杆
export function updatePoleApi(data) {
  return request({
    url: "/pole/info",
    method: "put",
    data,
  });
}
