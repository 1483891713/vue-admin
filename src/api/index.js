export * from "@/api/building";

export * from "@/api/enterprise";

export * from "@/api/user";

export * from "@/api/car";

export * from "@/api/role";

export * from "@/api/Pole";

export * from "@/api/employee";

export * from "@/api/work.js";

export * from "@/api/propety";
