import request from '@/utils/request'

/**
 * 获取规则列表
 * @param { page, pageSize} params
 * @returns
 */
export function getInfoAPI() {
  return request({
    url: '/home/workbench/info'
  })
}
