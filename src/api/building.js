import request from "@/utils/request";

//# 获取企业相关的数据
export function getBuildingListAPI(params) {
  return request({
    url: "/park/building",
    method: "GET",
    params,
  });
}

//楼宇新增
export function insertBuilding(data) {
  return request({
    url: "/park/building",
    method: "POST",
    data,
  });
}

//编辑楼宇
export function updateBuilding(data) {
  return request({
    url: "/park/building",
    method: "PUT",
    data,
  });
}

//删除楼宇的信息
export function deleteBuilding(id) {
  return request({
    url: `park/building/${id}`,
    method: "DELETE"
  });
}
