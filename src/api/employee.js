import request from "@/utils/request";

//#查询员工列表
export function getEmployeeListAPI(params) {
  return request({
    url: "/park/sys/user",
    params,
  });
}

//删除指定员工
export function deleteEmployeeById(id) {
  return request({
    url: `/park/sys/user/${id}`,
    method: "DELETE",
  });
}

//添加员工
export function appendEmployeeAPi(data) {
  return request({
    url: `/park/sys/user`,
    method: "post",
    data,
  });
}

//重置密码
export function resetPasswordApi(id) {
  return request({
    url: `/park/sys/user/resetPwd`,
    method: "post",
    data: {
      id,
    },
  });
}

//查看员工详情
export function getEmployeeDetailApi(id) {
  return request({
    url: `/park/sys/user/${id}`,
  });
}

//编辑员工信息
export function updataeEmployeeApi(data) {
  return request({
    url: `/park/sys/user`,
    method: "put",
    data,
  });
}
