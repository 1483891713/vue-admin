import request from "@/utils/request";

/**
 * 获取物业费列表
 * @param {*} params
 * @returns
 */
export function getPropetyListApi(params) {
  return request({
    url: "/park/propertyfee",
    params,
  });
}
/**
 * 根据id删除指定数据
 * @param {*} id
 * @returns
 */
export function deletePropetyInfoApi(id) {
  return request({
    url: `/park/propertyfee/${id}`,
    method: "DELETE",
  });
}

/**
 * 查询所有企业
 * @returns 查询所有企业
 */
export function getAllEnterpriseListApi() {
  return request({
    url: "/park/all/enterprise",
  });
}
/**
 * 查询所有楼宇
 */
export function getAllBuildingListApi() {
  return request({
    url: "/park/all/building",
  });
}

/**
 *  计算支付金额
 * @param {*} data
 * @returns
 */
export function countePayResultApi(data) {
  console.log(data);
  return request({
    url: "/park/propertyfee/pre/payment",
    method: "post",
    data,
  });
}

/**
 * 添加账单
 * @param {*} data
 * @returns
 */
export function appendBillApi(data) {
  return request({
    url: "/park/propertyfee",
    method: "post",
    data,
  });
}

/***
 * 查看账单详情
 */
export function getPropetyDdetails(id) {
  return request({
    url: `/park/propertyfee/${id}`,
  });
}
