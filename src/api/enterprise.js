import request from "@/utils/request";

//# 获取企业相关的数据
export function getExterpriseListAPI(params) {
  return request({
    url: "/park/enterprise",
    method: "GET",
    params,
  });
}

//#查询企业所属行业列表
export function getIndustryListAPI() {
  return request({
    url: "/park/industry",
  });
}

//上传功能
export function uploadFile(data) {
  return request({
    url: "/upload",
    method: "POST",
    data,
  });
}

//新增企业管理
export function insertExterprise(data) {
  return request({
    url: "/park/enterprise",
    method: "POST",
    data,
  });
}

// 获取指定企业的详情信息
export function getExterpriseDetails(id) {
  return request({
    url: `/park/enterprise/${id}`,
  });
}
//编辑企业指定的数据
export function editExterprise(data) {
  return request({
    url: "/park/enterprise",
    method: "PUT",
    data,
  });
}

export function deleteExterprise(id) {
  return request({
    url: `/park/enterprise/${id}`,
    method: "DELETE",
  });
}

/**
 * 获取合同列表
 * @param {*} id
 * @returns
 */
export function getRentListAPI(id) {
  return request({
    url: `/park/enterprise/rent/${id}`,
  });
}


/**
 * 获取楼宇列表
 * @param {*} id
 * @returns
 */
export function getRentBuildListAPI() {
  return request({
    url: '/park/rent/building'
  })
}


/**
 * 创建合同
 * @param {*}
 * @returns
 */
export function createRentAPI(data) {
  return request({
    url: '/park/enterprise/rent',
    method: 'POST',
    data
  })
}


/**
 * 退租
 * @param {合同id} rentId
 * @returns
 */

export function outRentAPI(rentId) {
  return request({
    url: `/park/enterprise/rent/${rentId}`,
    method: 'PUT'
  })
}
