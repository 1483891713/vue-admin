import { login } from "@/api/user";
import { getProfileAPI } from "@/api/index";
import { setToken, getToken, removeToken } from "@/utils/auth";
export default {
  namespaced: true,
  state: {
    token: getToken(),
    userInfo: {}, //用户信息
    // TODO: cookies 存储token
  },
  mutations: {
    setLoginToken(state, token) {
      state.token = token;

      //# 将token 保存到cookies中
      setToken(token);
    },
    clearLoginToken(state) {
      state.token = "";
      state.userInfo = {};
      removeToken();
    },

    setUserInfo(state, userInfo) {
      state.userInfo = userInfo;
    },
  },
  actions: {
    //TODO: 请求登录接口
    async loginAction(store, loignInfo) {
      let res = await login(loignInfo);
      store.commit("setLoginToken", res.data.token);
    },

    async getUserInfoAction(context) {
      //获取用户的数据
      const res = await getProfileAPI();

      context.commit("setUserInfo", res.data);

      return res.data.permissions;
    },
  },
};
