import { constantRoutes } from "@/router";
import { resetRouter } from "@/router/index";
export default {
  namespaced: true,
  state: {
    menuList: [], //最终菜单的集合
  },

  mutations: {
    setMenuList(state, asyncRoutes) {
      state.menuList = [...constantRoutes, ...asyncRoutes];
    },

    /**
     * 还原路由信息
     * @param {*} state
     */
    removeMenuList(state) {
      state.menuList = [...constantRoutes];
      // 重置路由
      resetRouter();
    },
  },
};
