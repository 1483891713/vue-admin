// 放置全局指令
import Vue from "vue";
import store from "@/store";
Vue.directive("auth-btn", {
  inserted: function (el, binding) {
    // el: 指令绑定的dom元素
    // bingding.value: 指令绑定的数据

    const btnPerms = binding.value;

    const permissions = store.state.user.userInfo.permissions;

    if (!permissions.includes(btnPerms)) {
      el.remove();
    }
  },
});
