import router from "./router";
import { getToken } from "./utils/auth";
import store from "./store";
import { routerList } from "@/router/asyncRoutes";

const WHITE_LIST = ["/login", "/404"];

router.beforeEach(async (to, from, next) => {
  let token = getToken();
  if (token) {
    next();

    if (!store.state.user.userInfo.id) {
      let permission = await store.dispatch("user/getUserInfoAction");

      // 2.1 处理一级菜单权限数据 把第一个冒号前面的字段都放到一个数组中 然后去重
      const firstRoutes = getFirstRoute(permission);
      const secondRoutes = getSecondRoute(permission);

      // const menuPerms = getMenuList();
      // console.log(firstRoutes, secondRoutes);
      // const filterRoute = routerList.filter((route) =>
      //   firstRoutes.includes(route.permission)
      // );

      // const finalRoutes = filterRoute.map((route) => {
      //   return {
      //     ...route,
      //     children: route.children.filter((item) =>
      //       secondRoutes.includes(item.permission)
      //     ),
      //   };
      // });
      const finalRoutes = filterRoutes(routerList, firstRoutes, secondRoutes);
      finalRoutes.forEach((item) => {
        router.addRoute(item);
      });

      store.commit("menu/setMenuList", finalRoutes);
    }

    // console.log(finalRoutes);
  } else {
    if (WHITE_LIST.includes(to.path)) {
      next();
    } else {
      next("/login");
    }
  }
});

function getFirstRoute(perms) {
  const list = perms.map((item) => {
    let splitList = item.split(":");

    return splitList[0];
  });

  return [...new Set(list)];
}

function getSecondRoute(perms) {
  const list = perms.map((item) => {
    let splitList = item.split(":");

    return `${splitList[0]}:${splitList[1]}`;
  });

  return [...new Set(list)];
}

function filterRoutes(asyncRoutes, firstPerms, secondPerms) {
  if (firstPerms.includes("*")) {
    return asyncRoutes;
  }
  const filterRoute = asyncRoutes.filter((route) =>
    firstPerms.includes(route.permission)
  );

  const finalRoutes = filterRoute.map((route) => {
    return {
      ...route,
      children: route.children.filter((item) =>
        secondPerms.includes(item.permission)
      ),
    };
  });
  return finalRoutes;
}

function getMenuList(perms) {
  let list = perms.map((item) => {
    let splitList = item.split(":");
    return {
      menus: splitList[0],
      smenus: `${splitList[0]}:${splitList[1]}`,
    };
  });

  return [...new Set(list)];
}

/**
 * 1. 将动态路由与静态路由进行分离
 * 2. 根据后端接口获取对应用户的权限，
 * 3. 将权限的数据分解出一级路由与二级路由（简写形式）
 * 4. 分解出来的简写形式与实际路由配置，进行匹配，得到一个完成成的一级路由信息
 * 5. 将一级路由的信息解析，匹配出满足条件的一级路由和二级路由信息
 * 6. 最后动态添加到路由表中
 */
