function transformV1(data) {
  if (Array.isArray(data) && data.length > 0)
    return data.map((item) => item * 3);
}

function transformV2(list) {
  if (Array.isArray(list) && list.length > 0) {
    return list.map((item) => {
      if (typeof item === "object" && Object.keys(item).length > 0) {
        return { ...item, name: "" };
      }
    });
  }
}

// let list = [1, 2, 3];
// console.log(transformV1(list));

// let list2 = [
//   { sex: "男", height: 180 },
//   { sex: "男", height: 180, address: "北京" },
// ];
// console.log(transformV2(list2));
