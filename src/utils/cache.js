export function getCache(key) {
  let value = localStorage.getItem(key);
  if (value) {
    return JSON.parse(value);
  }
}

export function setCache(key, value) {
  if (value) {
    localStorage.setItem(key, JSON.stringify(value));
  }
}

export function removeCache(key) {
  localStorage.removeItem(key);
}
