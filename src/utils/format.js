//设置禁用
export function addDisabled(treeList) {
  treeList.forEach((item) => {
    item.disabled = true;
    if (item.children) {
      addDisabled(item.children);
    }
  });
}
