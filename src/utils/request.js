import axios from "axios";
import { getToken } from "./auth";
import { Message } from "element-ui";
const service = axios.create({
  baseURL: "https://api-hmzs.itheima.net/v1/",
  timeout: 5000, // request timeout
});

// 请求拦截器
service.interceptors.request.use(
  (config) => {
    const token = getToken();

    //# 将token绑定到请求头中
    if (token) {
      config.headers.Authorization = token;
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

// 响应拦截器
service.interceptors.response.use(
  (response) => {
    return response.data;
  },
  (error) => {
    //TODO : 错误拦截
    if (error.response.data.msg) {
      Message.error(error.response.data.msg);
    }
    return Promise.reject(error);
  }
);

export default service;
